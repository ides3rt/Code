#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: Using Rofi to view Arch Wiki
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Code
# Last Modified: 2022-06-04
#------------------------------------------------------------------------------
# Dependencies:
#       arch-wiki-docs
#       bash
#       coreutils
#       findutils
#       menu
#
# Optional Dependencies:
#       links (TTY web-browser if $BROWSER isn't presented)
#       xdg-utils (GUI web-browser if $BROWSER isn't presented)
#------------------------------------------------------------------------------

declare -r prg=${0##*/}
declare -r wiki_dir=/usr/share/doc/arch-wiki/html/en

die ()
{
	echo "$prg: $2" >&2
	(( $1 )) &&
		exit $1
}

((EUID)) ||
	die 1 'denied to run as root.'

declare -i no_deps=0
for dep in find menu sort; {
	type -P "$dep" &>/dev/null || {
		die 0 "dependency, \`$dep\`, not met."
		(( no_deps++ ))
	}
}

(( no_deps )) &&
	die 1 "$no_deps dependency(s) missing, aborted."
unset -v dep no_deps

[[ -d $wiki_dir && -r $wiki_dir ]] ||
	die 1 "$wiki_dir: not found or unreadable."

cd -- "$wiki_dir"

docs=$(find * -regextype 'posix-extended' -type f -name '*.html' \
	-not -regex '(.*/)?([0-9A-Za-z]+:)?[0-9a-z]+\.html')

docs=${docs//_/ }
docs=${docs//.html}

user_doc=$(sort <<< "$docs" | menu --prompt='Arch Wiki Docs')

[[ -n $user_doc ]] ||
	exit 0

user_doc=$wiki_dir/${user_doc// /_}.html

cd -- "$OLDPWD"

if [[ -n $BROWSER ]]; then
	$BROWSER "$user_doc"

elif [[ -n $DISPLAY ]]; then
	type -P xdg-open &>/dev/null ||
		die 1 'dependency, `xdg-open`, not met.'
	xdg-open "$user_doc"

elif type -P links &>/dev/null; then
	links "$user_doc"
fi
